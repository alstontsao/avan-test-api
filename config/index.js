const DEBUG = process.env.NODE_ENV !== 'production'
const PORT = process.env.NODE_ENV === 'production' ? 29919 : 29919

module.exports = {
  DEBUG,
  PORT,
  VERSION: '1.0',
  SECRET: 'donottell',
}
