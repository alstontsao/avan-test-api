const KoaRouter = require('koa-router')
const jwt = require('jsonwebtoken')

const db = require('../../db')
const config = require('../../../config')

const router = new KoaRouter()

router.get('/me', async (ctx) => {
  ctx.assert(ctx.state.admin, 403)
  ctx.body = ctx.state.admin
})

router.post('/login', async (ctx) => {
  const { account, password } = ctx.request.body
  ctx.assert(account && password, 400)

  const [ admin ] = await db.knex('Admin').select(['id', 'name']).where({ account, password })
  ctx.assert(admin, 401, 'Oops!')
  admin.timestamp = +new Date()

  ctx.body = {
    token: jwt.sign({...admin}, config.SECRET),
    admin
  }
})

module.exports = {
  router
}
