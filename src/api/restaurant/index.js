const KoaRouter = require('koa-router')
const jwt = require('jsonwebtoken')

const db = require('../../db')
const config = require('../../../config')

const router = new KoaRouter()

router.get('/list', async (ctx) => {
  const { lat, lon, timeslots, name } = ctx.request.body
  ctx.body = await db.knex('Restaurant').select('*')
})

module.exports = {
  router
}
