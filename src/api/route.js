const router = require('koa-router')()
const { router: admin } = require('./admin')
const { router: user } = require('./user')
const { router: order } = require('./order')
const { router: restaurant } = require('./restaurant')
const { auth: adminMiddleware } = require('../middleware/auth')

router.use(async (ctx, next) => {
  try {
    await next()
  } catch (e) {
    console.error(e)
    if (e.sqlMessage) {
      e.message = null
    }
    ctx.body = { message: e.message || 'Internal Server Error' }
    ctx.status = e.status || 500
  }
})
router.use('/api/v1/user', user.routes())
router.use('/api/v1/order', order.routes())
router.use('/api/v1/restaurant', restaurant.routes())
router.use(adminMiddleware)
router.use('/api/v1/admin', admin.routes())


module.exports = router
