const KoaRouter = require('koa-router')
const jwt = require('jsonwebtoken')

const db = require('../../db')
const config = require('../../../config')

const router = new KoaRouter()

router.post('/google-login', async (ctx) => {
  const { googleID, name, avatarURL, email } = ctx.request.body

  const selectColumns = ['id', 'name', 'email', 'avatarURL', 'account']
  const [ user ] = await db.knex('User').select(selectColumns).where({ email })
  if (!user) {
    const result = await db.query('INSERT User SET ?', [{
      account: email, googleID, name, avatarURL, email
    }])
    console.log(result)
    const [newUser] = await db.knex('User').select(selectColumns).where({ id: result.insertId })
    ctx.body = newUser
  } else {
    ctx.body = {
      user
    }
  }
})

router.get('/me', async (ctx) => {
  ctx.assert(ctx.state.admin, 403)
  ctx.body = ctx.state.admin
})

module.exports = {
  router
}
