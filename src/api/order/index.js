const db = require('../../db')
const KoaRouter = require('koa-router')
const router = new KoaRouter()

module.exports = {
  router
}
