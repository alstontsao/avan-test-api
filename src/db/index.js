const mysql = require('mysql')
const instance = {
  host: 'avan-test-db.appccic.com',
  user: 'avantest',
  password: 'avantest',
  port: 3306,
  database: 'avan'
}
const pool = mysql.createPool({...instance,
  charset: 'UTF8MB4_GENERAL_CI',
  connectionLimit: 64,
  multipleStatements: true,
  supportBigNumbers: true,
  dateStrings: 'DATETIME',
  queueLimit: 0,
})

if (!pool) {
  throw (Error('Mysql create pool fail.'))
}

const knex = require('knex')({
  client: 'mysql',
  connection: instance,
  pool: { min: 0, max: 64 }
})

module.exports = {
  getConnection() {
    return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
        if(err) {
          reject(err)
        } else {
          resolve(connection)
        }
      })
    })
  },
  query(queryString, queryParams, options) {
    return new Promise((resolve, reject) => {
      if (!queryString || typeof (queryString) !== typeof ('')) return reject(Error('invalid query string'))
      const sql = { sql: this.format(queryString, queryParams) }
      return pool.query(Object.assign(sql, options), (err, result) => {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
    })
  },
  format(queryString, queryParams) {
    return mysql.format(queryString, queryParams)
  },
  knex
}
