const { verify } = require('jsonwebtoken')

const db = require('../db')
const config = require('../../config')
const getToken = require('../utils/auth')

exports.auth = async (ctx, next) => {
  try {
    const token = getToken(ctx)
    if(!token) return next()
    const { id } = verify(token, config.SECRET)
    if(!id) return next()
    const [admin = null] = await db.knex('Admin').select(['id', 'name']).where({ id })
    ctx.state.admin = admin
    return next()
  } catch (err) {
    console.error(err)
    return next()
  }
}