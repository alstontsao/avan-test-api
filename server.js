const config = require('./config')
const app = require('./src/app.js')
const notifier = require('node-notifier')
const http = require('http')

const init = (port) => {
  const server = http.createServer(app.callback())
  server.listen(port)
  server.maxConnections = 10

  if (config.DEBUG) {
    const tag = config.DEBUG ? '[DEBUG]' : '[PRODUCTION]'
    console.log(`${tag} API Server [${config.VERSION}] Started at port: ${port}`)
    notifier.notify({
      title: 'API Server Started',
      message: `Port: ${port}`,
      icon: './ok.png',
      sound: true
    })
  }

  return server
}

module.exports = init(config.PORT)
